<?php
require_once('Animal.php');
require_once('Frog.php');
require_once('Ape.php');

$sheep = new Animal("shaun");

echo "Namanya adalah : $sheep->name <br>";
echo "Jumlah Kakinya ialah : $sheep->legs <br>";
echo "Apakah berdarah dingin : $sheep->cold_blooded <br> <br>";


$katak = new Frog("Buduk");
echo "Namanya adalah : $katak->name <br>";
echo "Jumlah kakinya ialah : $katak->legs <br>";
echo "Apakah berdarah dingin : $katak->cold_blooded <br>";
echo $katak->jump() . " Dia sedang melompat <br><br>";


$ape = new Ape("Sungokong");
echo "Namanya adalah : $ape->name <br>";
echo "Jumlah kakinya ialah : $ape->legs <br>";
echo "Apakah berdarah dingin : $ape->cold_blooded <br>";
echo $ape->yell() . " Dia sedang bersuara <br><br>";
